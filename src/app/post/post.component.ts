import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
})
export class PostComponent {
  title: string = 'List Of Posts';
  messagePost: string = 'Message Post';
  @Input() fromParent: any;
  parentpostMessage?: string = 'Message coming from post parent component';
  childMessage?: string = 'From child component';
  // sending data from child to parent where there is an event, using the @Output decorator and Event Emitter - ideal when we want to share data changes that occur on things like button clicks, form entries and other user events.// EventEmitter() class must be added to the @Output decorator to emit event and notify the parent of the change.
  outputChildMessage: string = 'Message from Child Component via Output';
  @Output() messageEvent = new EventEmitter<string>();
  sendMessage() {
    this.messageEvent.emit(this.outputChildMessage);
  }
}

// how this transfer of data via @Output decorator and Event Emitter works -
